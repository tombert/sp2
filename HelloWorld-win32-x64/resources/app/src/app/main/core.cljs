(ns app.main.core
  (:require ["electron" :refer [app BrowserWindow crashReporter]]))

(def elc (js/require "electron"))
(def global-shortcut (.-globalShortcut elc))
(def clipboard (.-clipboard elc))

(def main-window (atom nil))

(defn init-browser []
  (reset! main-window (BrowserWindow.
                        (clj->js {:width 800
                                  :height 600
                                  :webPreferences
                                  {:nodeIntegration true}})))
  ; Path is relative to the compiled js file (main.js in our case)
  (.loadURL ^js/electron.BrowserWindow @main-window (str "file://" js/__dirname "/public/index.html"))
  (.on ^js/electron.BrowserWindow @main-window "closed" #(reset! main-window nil)))

(defn main []
  ; CrashReporter can just be omitted
  (.start crashReporter
          (clj->js
           {:companyName "MyAwesomeCompany"
            :productName "MyAwesomeApp"
            :submitURL "https://example.com/submit-url"
            :autoSubmit false}))

  (.on app "window-all-closed" #(when-not (= js/process.platform "darwin")
                                  (.quit app)))
  (.on app "ready" init-browser)
  
  
  (.on app "ready" (fn []
       (. global-shortcut register "CommandOrControl+Shift+D" 
            (fn []

              (let []
              (. (. ^js/electron.BrowserWindow @main-window -webContents ) send "kill-speech" "stop"))))
       (. global-shortcut register "CommandOrControl+D" 
            (fn []

              (let [cb (. clipboard readText "selection")]
              (. (. ^js/electron.BrowserWindow @main-window -webContents ) send "send-message" cb)))))))
