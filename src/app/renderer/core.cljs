(ns app.renderer.core
  (:require [reagent.core :refer [atom]]
            [reagent.dom :as rd]))

(enable-console-print!)
(defonce input-string (atom ""))

(def voices (. (. js/window  -speechSynthesis ) getVoices))

;(js/console.log (str voices))

(def c (js/require "electron"))
(def ipc (.-ipcRenderer c))
(defonce state (atom 0))

(defn play-text [in-string]
  (let [fart (. js/window -SpeechSynthesisUtterance)
        su (fart. in-string)]

  (. (. js/window -speechSynthesis ) speak su)))

; (. globalShortcut -register "CommandOrControl+X" 
;    (fn []
;      (play-text "howdy")
;      ))
(defn stop-text []
  (. (. js/window -speechSynthesis ) cancel ))

(. ipc on "kill-speech" 
      (fn [event stopevent]
        (js/console.log (str "stop message: " stopevent))
        (stop-text)))

(. ipc on "send-message" 
   (fn [event message]
     (js/console.log (str "my message: " message))
     (play-text message)
     ))

  ; ipcRenderer.on('openDir', (event, dirPath) => {
  ;  if (dirPath !== this.state.rootDirPath) { 
  ;   this.setFileTree(dirPath);
  ;  }
  ; }),

(defn yoyo [i]
  (let [v (.-value (.-target i))]
  (js/console.log (str "Selected Voice: " (.-name v)  ) )))

(defn real-root []
  
  [:div
   [:div 
    [:button
     {:on-click (fn [e]
                 (play-text @input-string) 
                  )}
     "Play the stuff in the box" ]
    [:button
     {:on-click (fn [e]
                 (stop-text ) 
                  )}
     "Stop" ]
    ]

   [:div 
    [:textarea 
     {:on-change (fn [e]
                   (reset! 
                     input-string 
                     (.-value (.-target e))
                   )) }] ] 
   
   [:div "Voice: " [:select {:on-change yoyo} (->> voices (map (fn [voice]
                            [:option {:key (.-name voice) :value voice} (.-name voice)]
                            
                            )))]]
   ])

(defn root-component []
  [:div
   [:div.logos
    [:img.electron {:src "img/electron-logo.png"}]
    [:img.cljs {:src "img/cljs-logo.svg"}]
    [:img.reagent {:src "img/reagent-logo.png"}]]
   [:button
    {:on-click #(swap! state inc)}
    (str "Clicked " @state " times")]])

(defn ^:dev/after-load start! []
  (rd/render
   [real-root]
   (js/document.getElementById "app-container")))
